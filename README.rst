BCFTools Consensus GeneFlow App
===============================

Version: 1.9-01

This GeneFlow app wraps the BCFTools Consensus tool and also uses seqtk utilities to reformat and rename the output consensus sequences.

Inputs
------

1. input: VCF file with one or more samples.

2. reference_sequence: Reference sequence fasta.

Parameters
----------

1. output: Directory that contains an output fasta.gz file for each sample in the VCF.

